Parameter monotonic : forall {A B} (leA : A -> A -> Prop) (leB : B -> B -> Prop),
  (A -> B) -> Prop.

Hypothesis monotonic_cst : forall A B (leA : A -> A -> Prop) (leB : B -> B -> Prop),
  forall (b:B), monotonic leA leB (fun _ : A => b).

Hint Extern 0 (monotonic _ _ (fun _ => ?x)) =>
  simple apply monotonic_cst : mymonotonic.

Parameter (foo : nat -> nat).

Goal (forall a, monotonic le le (fun _ => foo a)).
  typeclasses eauto with mymonotonic.
Qed.


Hint Extern 999999 (monotonic _ _ _) =>
match goal with
| |- _ -> _ => fail 1
| _ => shelve
end : mymonotonic.

Hint Extern 0 (monotonic _ _ (fun _ => ?x)) =>
  simple apply monotonic_cst : mymonotonic2.

Hint Extern 1 =>
  match goal with |- forall _, _ => intro end : mymonotonic2.

Hint Extern 9999 => shelve : mymonotonic2.

Goal (forall a, monotonic le le (fun _ => foo a)).
  unshelve (typeclasses eauto with mymonotonic2).

  unshelve (typeclasses eauto with mymonotonic).
  admit.
Admitted.