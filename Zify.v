Require Import ZArith.
Open Scope Z.

Goal Z.max 0 (1 + Z.max 0 (Z.max 0 (Z.max 0 0))) = 1.

Goal (0 = Z.max 2 0).
  omega.