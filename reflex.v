Require Import ZArith.
Open Scope Z_scope.
Require Import List.
Import ListNotations.

Inductive expr :=
| Var : expr
| E0 : expr
| EAdd : expr -> expr -> expr
| EMax : expr -> expr -> expr
| EAbstr : Z -> expr.

Ltac reflect z E cont :=
  lazymatch E with
  | 0 => cont constr:(E0)
  | ?x + ?y =>
    reflect z x ltac:(fun ex =>
    reflect z y ltac:(fun ey =>
    cont constr:(EAdd ex ey)))
  | Z.max ?x ?y =>
    reflect z x ltac:(fun ex =>
    reflect z y ltac:(fun ey =>
    cont constr:(EMax ex ey)))
  | z => cont constr:(Var)
  | ?x => cont constr:(EAbstr x)
  end.

Fixpoint denote expr x :=
  match expr with
  | Var => x
  | E0 => 0
  | EAdd e1 e2 => (denote e1 x) + (denote e2 x)
  | EMax e1 e2 => Z.max (denote e1 x) (denote e2 x)
  | EAbstr z => z
  end.

Inductive eq :=
  | EQ : expr -> expr -> eq.

Definition denote_eq e :=
  match e with
  | EQ e1 e2 => forall x, denote e1 x = denote e2 x
  end.

Axiom Ax : forall e1 e2, e1 = e2 -> denote_eq (EQ e1 e2).

Require Import Template.All.

Goal forall x y, x + y + 0 = x + y.
  intro x.
  Fail Fail apply (@Ax (EAdd (EAdd (EAbstr x) Var) E0) (EAdd (EAbstr x) Var)).

  Fail match goal with |- forall y, ?X = _ =>
    reflect y X ltac:(fun z => pose z)
  end.

  match goal with |- ?X =>
    quote_term X ltac:(fun t => pose t)
  end.

  Ltac myquote_of_coqterm t cont :=
    match t with
    | tApp (tConst "Coq.ZArith.BinIntDef.Z.add" []) [?x; ?y] =>
      myquote_of_coqterm x ltac:(fun e1 =>
      myquote_of_coqterm y ltac:(fun e2 =>
      cont constr:(EAdd e1 e2)))
    | tApp (tConst "Coq.ZArith.BinIntDef.Z.max" []) [?x; ?y] =>
      myquote_of_coqterm x ltac:(fun e1 =>
      myquote_of_coqterm y ltac:(fun e2 =>
      cont constr:(EMax e1 e2)))
    | tRel 0 =>
      cont constr:(Var)
    | tInd {| inductive_mind := "Coq.Numbers.BinNums.Z"; inductive_ind := 0 |} [] =>
      cont constr:(E0)
    | ?x =>
      denote_term x ltac:(fun dx =>
      cont constr:(EAbstr dx))
    end.

  intros.
  match goal with |- ?X = _ =>
    quote_term X ltac:(fun t =>
    myquote_of_coqterm t ltac:(fun z => pose z))
  end.