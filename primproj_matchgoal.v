Set Primitive Projections.

Record myprod A B :=
  mypair { myfst : A ; mysnd : B }.

Arguments mypair {_ _} _ _.
Parameter P : (myprod nat nat -> nat) -> Prop.
Unset Printing Primitive Projection Compatibility.

Set Printing All.
Goal P (fun '(mypair x y) => y).
  Fail match goal with |- P (fun '(mypair x y) => y) => idtac end.
  Fail match goal with |- P (fun pat : myprod nat nat => mysnd nat nat pat) => idtac end.



Parameter Q : (nat * nat -> nat) -> Prop.
Hypothesis L1 : forall f, Q (fun '(x,_) => f x).
Hypothesis L2 : forall f, P (fun '(mypair x _) => f x).

Goal Q (fun '(x,_) => x).
  apply L1.

Goal P (fun '(mypair x _) => x).
  Fail apply L2.