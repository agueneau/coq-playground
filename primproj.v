Set Primitive Projections.

Record myprod A B :=
  mypair { myfst : A ; mysnd : B }.

Unset Primitive Projections.

Parameter P : (myprod nat nat -> nat) -> Prop.

Hypothesis L : forall cst, P (fun _ => cst).

Arguments mypair {A B} _ _.
Notation "( x , y , .. , z )" := (mypair .. (mypair x y) .. z) : myprod_scope.
Delimit Scope myprod_scope with myprod_key.

Hypothesis L2 :
  forall f g,
    P f -> P g ->
    P (fun p => f p + g p).

Unset Printing Primitive Projection Parameters.
Unset Printing Primitive Projection Compatibility.

Section Test.

Open Scope myprod_scope.

Goal P (fun p => let 'mypair x y := p in 3).
Goal P (fun p => let '(x, y) := p in 3).
Goal P (fun '(x, y) => 3).

Arguments myfst {A B} _.
Arguments mysnd {A B} _.

(* Goal P (fun p => p.(myfst) + p.(mysnd)). *)
Goal P (fun '(x,y) => x + y).

Goal P (fun p => match p with (x, y) => x + y end).

Goal P (fun '(x, y) => x+y).
  apply L2.

Goal P (fun p => match p with mypair x y => 3 end).
  apply L.

Abort All.

End Test.
