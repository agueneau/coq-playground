Require Import ZArith.
Open Scope Z_scope.
Require Import List.
Require Import Template.All.
Import ListNotations MonadNotation.

Inductive expr :=
| EVar : nat -> expr
| EAdd : expr -> expr -> expr
| EMul : expr -> expr -> expr
| EAbstr : Z -> expr.

Ltac reflect_expr term cont :=
  lazymatch term with
  | tApp (tConst "Coq.ZArith.BinIntDef.Z.add" []) [?x; ?y] =>
    reflect_expr x ltac:(fun e1 =>
    reflect_expr y ltac:(fun e2 =>
    cont constr:(EAdd e1 e2)))
  | tApp (tConst "Coq.ZArith.BinIntDef.Z.mul" []) [?x; ?y] =>
    reflect_expr x ltac:(fun e1 =>
    reflect_expr y ltac:(fun e2 =>
    cont constr:(EMul e1 e2)))
  | tApp (tConst "Coq.ZArith.BinIntDef.Z.sub" []) [?x; ?y] =>
    reflect_expr x ltac:(fun e1 =>
    reflect_expr y ltac:(fun e2 =>
    cont constr:(EAdd e1 (EMul (EAbstr (-1)) e2))))
  | tRel ?n =>
    cont constr:(EVar n)
  | ?x =>
    denote_term x ltac:(fun dx =>
    cont constr:(EAbstr dx))
  end.

Ltac skip_ex t cont :=
  lazymatch t with
  | tApp (tInd {| inductive_mind := "Coq.Init.Logic.ex"; inductive_ind := 0 |} [])
         [tInd {| inductive_mind := "Coq.Numbers.BinNums.Z"; inductive_ind := 0 |} [];
          tLambda (nNamed _) _ ?body] =>
    skip_ex body cont
  | _ =>
    cont t
  end.

Ltac quote_under_ex term cont :=
  quote_term term ltac:(fun t => skip_ex t cont).

Ltac reflect_ineqs term cont :=
  lazymatch term with
  | tApp (tInd {| inductive_mind := "Coq.Init.Logic.and"; inductive_ind := 0 |} [])
      [?x; ?y] =>
    reflect_ineqs x ltac:(fun rx =>
    reflect_ineqs y ltac:(fun ry =>
    cont constr:(rx ++ ry)))
  | tApp (tConst "Coq.ZArith.BinInt.Z.le" []) [?x; ?y] =>
    reflect_expr x ltac:(fun e1 =>
    reflect_expr y ltac:(fun e2 =>
    cont constr:([(e1, e2)])))
  | tApp (tConst "Coq.ZArith.BinInt.Z.ge" []) [?x; ?y] =>
    reflect_expr x ltac:(fun e1 =>
    reflect_expr y ltac:(fun e2 =>
    cont constr:([(e2, e1)])))
  end.

Goal exists x y z, x >= 0 /\ (42 <= y /\ x <= 5) /\ z >= 3.
  match goal with |- ?X => quote_under_ex X ltac:(fun t =>
    reflect_ineqs t ltac:(fun rt =>
      let x := eval cbv in rt in
      pose x
    ))
  end.
Abort.

Fixpoint assoc_add
  (l: list (nat * expr)) (x: nat) (y: expr): list (nat * expr)
:=
  match l with
  | [] => [(x, y)]
  | (u, v) :: l' =>
    if Nat.eq_dec x u then
      (u, EAdd v y) :: l'
    else
      (u, v) :: (assoc_add l' x y)
  end.

Fixpoint merge_add (l1 l2: list (nat * expr)): list (nat * expr) :=
  match l1 with
  | [] => l2
  | (x, y) :: l1' => merge_add l1' (assoc_add l2 x y)
  end.

Notation "'do' ( x , y ) <- c1 ;; c2" := (@bind _ _ _ _ c1 (fun '(x,y) => c2))
    (at level 100, c1 at next level, right associativity) : monad_scope.

Definition poly_cst_mul (p: list (nat * expr) * list expr) (k: expr): list (nat * expr) * list expr :=
  let '(m, c) := p in
  let m' := List.map (fun '(n, e) => (n, EMul e k)) m in
  let c' := List.map (fun e => EMul e k) c in
  (m', c').

Fixpoint addl (l: list expr): expr :=
  match l with
  | [] => EAbstr 0
  | [x] => x
  | x :: xs => EAdd x (addl xs)
  end.

Fixpoint poly (e: expr): option (list (nat * expr) * list expr) :=
  match e with
  | EAdd e1 e2 =>
    do (m1, c1) <- poly e1 ;;
    do (m2, c2) <- poly e2 ;;
    ret (merge_add m1 m2, c1 ++ c2)
  | EMul e1 e2 =>
    do (m1, c1) <- poly e1 ;;
    do (m2, c2) <- poly e2 ;;
    match (m1, m2) with
    | ([], _) => ret (poly_cst_mul (m2, c2) (addl c1))
    | (_, []) => ret (poly_cst_mul (m1, c1) (addl c2))
    | (_::_, _::_) => None
    end
  | EVar n => ret ([(n, EAbstr 1)], [])
  | EAbstr x => ret ([], [e])
  end.

Goal forall cst, exists x y z u,
  1 + (x + (((cst * y) + z) + u) + ((x * (cst + 2) + x) + (((y + u) * cst) + cst + cst))) <= 0.

intro.
match goal with |- ?X =>
  quote_under_ex X ltac:(fun t =>
    reflect_ineqs t ltac:(fun rt =>
      let rt' := eval cbv in rt in
      lazymatch rt' with
      | [(?e, _)] =>
        let p := eval cbv in (poly e) in
        pose p
      end
    ))
end.
