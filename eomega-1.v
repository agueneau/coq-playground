Require Import Omega.
Require Import Arith.

(* For n t == t 0 || t 1 || .. || t (n - 1) *)
Ltac For n t :=
  match n with
    0 => idtac
  | S ?n' => (For n' t || t n')
  end.

(* run n t == t n || t (1 + n) || t (2 + n) || ..  *)
Ltac For_inf n t :=
  t n ||
    let n' := (eval compute in (1 + n)) in
    For_inf n' t.

(* try tactic [t] after instantiating existentials in the cube
   [0, n-1]^(number of "exists") *)
Ltac iter_cube t n :=
  lazymatch goal with
  | |- ex   _ => For n ltac:(fun i => exists i; iter_cube t n)
  | |- sig  _ => For n ltac:(fun i => exists i; iter_cube t n)
  | |- sigT _ => For n ltac:(fun i => exists i; iter_cube t n)
  | |- _ => t
  end.

(* try tactic [t] after instantiating existentials in the cone
   (x_1 + ... + x_(number of "exists")) < n *)
Ltac iter_cone t n :=
  lazymatch goal with
  | |- ex   _ => For n ltac:(fun i => exists i; let n' := eval compute in (n - i) in iter_cone t n')
  | |- sig  _ => For n ltac:(fun i => exists i; let n' := eval compute in (n - i) in iter_cone t n')
  | |- sigT _ => For n ltac:(fun i => exists i; let n' := eval compute in (n - i) in iter_cone t n')
  | |- _ => t
  end.

(* try tactic [t] after instantiating existentials in the plane
   (x_1 + ... + x_(number of "exists")) == n *)
Ltac iter_plane t n :=
  let Sn := (eval compute in (1 + n)) in
  lazymatch goal with
  | |- ex   _ => For Sn ltac:(fun i => exists i; let n' := eval compute in (n - i) in iter_plane t n')
  | |- sig  _ => For Sn ltac:(fun i => exists i; let n' := eval compute in (n - i) in iter_plane t n')
  | |- sigT _ => For Sn ltac:(fun i => exists i; let n' := eval compute in (n - i) in iter_plane t n')
  | |- _ => match n with 0 => t | _ => fail end
  end.

(* [iter_planes t] tries [t] on planes of increasing distance to 0.
   (Will run forever if [t] never succeeds.)  *)

Tactic Notation "iter_planes" tactic(t) :=
  For_inf 1 ltac:(iter_plane t).

(* [iter_planes t n] tries [t] on planes of distance to 0 from 0 to n-1 *)

Tactic Notation "iter_planes" tactic(t) constr(n) :=
  For n ltac:(iter_plane t).

(* [iter_cubes t n] tries [t] on cubes of side from 0 from 0 to n-1
   (faster than [iter_cube t n] in cases where the solution is close
   to 0 and n is large) *)

Tactic Notation "iter_cubes" tactic(t) constr(n) :=
  For n ltac:(iter_cube t).

(* [iter_cubes t] tries [t] on cubes of increasing side.  Will run
   forever if [t] never succeeds. *)

Tactic Notation "iter_cubes" tactic(t) :=
  For_inf 1 ltac:(iter_cube t).

(* Which of the above tactics/notations is faster depends on where the
   solutions typically are. Closer to the diagonal would mean cubes
   are faster.  But if one wants no redundancy in the exploration and
   never run [t] twice on the same instantiation, one should use
   [iter_planes t]. *)

(** Use [eomega] for most uses. *)

Tactic Notation "eomega" :=
  iter_cubes omega.

Tactic Notation "eomega" constr(n) :=
  let n' := (eval compute in (1 + n)) in
  iter_cube omega n'.

Tactic Notation "eomega" "sum" :=
  iter_planes omega.

Tactic Notation "eomega" "sum" constr(n) :=
  let n' := (eval compute in (1 + n)) in
  iter_cone omega n'.

(** If your goal is [exists a, exists b, ...], try:
  - [eomega] to try all instances
  - [eomega 5] to try all instances with a, b, .. <= 5
  - [eomega sum] to try all instances without redundancy
  - [eomega sum 5] to try instances with a + b + ... <= 5
*)

Goal exists a b, a = 5 /\ b = 5.
Proof.
  Fail Fail eomega.   (* success *)
  Fail      eomega 4. (* fail: 4 is not enough) *)
  Fail Fail eomega 5. (* success *)
  
  Fail Fail eomega sum.    (* success *)
  Fail      eomega sum 9.  (* fail: 9 is not enough *)
  Fail Fail eomega sum 10. (* success *)

  eomega.
Qed.

Goal 15 = 15.
Proof.
  eomega.
Qed.

Goal exists n, n = 15.
Proof.
  eomega.
Qed.

Goal exists a b c, a = 5 /\ b = 6 /\ c = 4 /\ a + c + b = b + a + c.
Proof.
  eomega.
Qed.

Goal exists x y, 3 * x * y = 12.
Proof.
  eomega.
Qed.

Goal exists x2 x3 x4 : nat, 0 <= x2 /\ 1 <= x4 /\ x4 <= x3 /\ S (S x4) <= x3 /\ 2 <= x2.
Proof.
  eomega.
Qed.

Goal forall x2 x3 x4 : nat,
    (0 <= x2 /\ 1 <= x4 /\ x4 <= x3 /\ S (S x4) <= x3 /\ 2 <= x2) ->
    x2 <= 2 ->
    x3 <= 2 ->
    x4 <= 2 ->
    False.
Proof.
  intros.
  omega.
Qed.

Goal exists a b, forall x, 6 * x + x + x = a * x + b.
  Time iter_planes ltac:(intros; ring).
  (* 0.2 s *)
Qed.

Goal exists a b c, forall x, (x + 2) * (x + 4) = a * x * x + b * x + c.
  Time iter_planes ltac:(intros; ring).
  (* 5 s (8 s with "iter_cubes" *)
Qed.

Goal exists a b c d, forall x y, (x + 2) * (y + 4) = a * x * y + b * x + c * y + d.
  Time iter_planes ltac:(intros; ring).
  (* 25 s *)
Qed.
