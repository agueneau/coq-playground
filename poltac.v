Require Import ZArith.
Open Scope Z_scope.
Require Import PolTac.PolTac.

Goal forall (a b c d m n z z0 : Z),
  0 <= m ->
  0 <= n ->
  2 * z + a * m + a + b * n + c * z0 + d + 1 <= a * m + b * n + b + c * z0 + d.
Proof.
  intros.