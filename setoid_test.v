Require Import Coq.Setoids.Setoid.

Parameter myrel : nat -> nat -> Prop.
Parameter myrel_reflexive : forall a, myrel a a.
Parameter myrel_transitive : forall a b c, myrel a b -> myrel b c -> myrel a c.

Add Relation nat myrel
  reflexivity proved by myrel_reflexive
  transitivity proved by myrel_transitive
  as myrel_preorder.

Parameter lemma : forall a b, myrel a b.

Goal forall a c, myrel a c.
intros a c.
rewrite lemma. (* works, replaces [a] by an evar. *) Undo.
rewrite lemma. rewrite <-lemma. (* works (??!), replaces [a] then [c] by evars. *) Undo. Undo.
rewrite <-lemma. (* doesn't work. *)


(* The following works, as a workaround, but is annoying to use, and requires
   writing [c] (which may be big) in the proof script. *)
rewrite <-lemma with (b := c).

Abort.