(*
Require Import Equations.Equations.

(* Equations Rtuple' (domain : list Type) : Type := *)
(*   Rtuple' nil := unit; *)
(*   Rtuple' (cons d nil) := d; *)
(*   Rtuple' (cons d ds) := prod (Rtuple' ds) d. *)

Equations Rtuple' (domain : list Type) : Type :=
  Rtuple' nil := unit ;
  Rtuple' (cons d ds) <= ds => {
    | nil => d ;
    | _ => prod (Rtuple' ds) d }.

Check Rtuple'_equation_2.

*)

Require Import Coq.Lists.List.
Import List.ListNotations.

Section With_extlib.

Require Import ExtLib.Generic.Func.

Parameter A B C D : Type.

Eval compute in (asFunc [A;B;C] bool).
Eval compute in (asTuple [A;B;C]).

Print Member.member.

Parameter foo : nat -> nat -> nat.
Eval compute in (fun p : nat * nat => let '(a, b) := p in foo (fst p) (snd p)).

End With_extlib.


Fixpoint Arrow (domain : list Type) (range : Type) : Type :=
  match domain with
    | nil => range
    | d :: ds => d -> Arrow ds range
end.

Eval compute in Arrow [A;B;C] bool.

Fixpoint Rarrow (domain : list Type) (range : Type) : Type :=
  match domain with
    | nil => range
    | d :: ds => Rarrow ds (d -> range)
end.

Eval compute in Rarrow [C;B;A] bool.

Fixpoint Rtuple (domain : list Type) : Type :=
  match domain with
  | nil => unit
  | d :: nil => d
  | d :: ds => prod (Rtuple ds) d
  end.

Eval compute in Rtuple [C;B;A].
Check ((A * B) * C)%type. (* (A * B * C) *)

Fixpoint Tuple (domain : list Type) : Type :=
  match domain with
  | nil => unit
  | d :: nil => d
  | d :: ds => prod d (Tuple ds)
  end.

Eval compute in Tuple [A;B;C].

(*
Fixpoint Rtuple2_aux (domain : list Type) (acc : Type) : Type :=
  match domain with
  | nil => acc
  | d :: ds => Rtuple2_aux ds (prod acc d)
  end.

Definition Rtuple2 (domain : list Type) : Type :=
  match domain with
  | nil => unit
  | d :: ds => Rtuple2_aux ds d
  end.

Eval compute in Rtuple2 [A;B;C].
*)

Fixpoint Let (domain : list Type) (range : Type) {struct domain}
         : (Rtuple domain) -> (Rtuple domain -> range) -> range
  :=
  match domain with
  | nil => fun t body => body tt
  | d :: ds =>
     let f := Let ds range in
     match ds return
        (Rtuple ds -> (Rtuple ds -> range) -> range) ->
        (Rtuple (d :: ds) -> (Rtuple (d :: ds) -> range) -> range)
     with
     | [] => fun _ t body => body t
     | _ =>
         fun f t body =>
         let '(t', x) := t in f t' (fun p' => body (p', x))
     end f
  end.

Lemma Let_eqn_1 : forall range t body,
  Let [] range t body = body tt.
Proof. intros. reflexivity. Qed.

Lemma Let_eqn_2 : forall d range t body,
  Let [d] range t body = body t.
Proof. intros. reflexivity. Qed.

Lemma Let_eqn_3 : forall d d' ds range t body,
  Let (d :: d' :: ds) range t body =
  (let '(t', x) := t in Let (d' :: ds) range t' (fun p' => body (p', x))).
Proof. intros. reflexivity. Qed.

(*
Fixpoint Let' (domain : list Type) (range : Type) {struct domain}
         : (Tuple domain) -> (Tuple domain -> range) -> range
  :=
  match domain with
  | nil => fun t body => body tt
  | d :: ds =>
     let f := Let' ds range in
     match
       ds as ds0 return
        (Tuple ds0 -> (Tuple ds0 -> range) -> range) ->
        (Tuple (d :: ds0) -> (Tuple (d :: ds0) -> range) -> range)
     with
     | [] => fun _ t body => body t
     | _ =>
         fun f t body =>
         let '(x, t') := t in f t' (fun p' => body (x, p'))
     end f
  end.
*)

Fixpoint App (domain : list Type) (range : Type) {struct domain}
         : (Arrow domain range) -> Rtuple domain -> range
  :=
  match domain with
  | nil => fun f x => f
  | d :: ds =>
    let Apprec := App ds range in
    match ds return
      ((Arrow ds range) -> Rtuple ds -> range) ->
      (Arrow (d :: ds) range) -> Rtuple (d :: ds) -> range
    with
    | [] => fun _ f x => f x
    | _ => fun Apprec f t => Apprec (f (snd t)) (fst t)
    end Apprec
  end.

(*
Fixpoint App' (domain : list Type) (range : Type) {struct domain}
         : (Arrow domain range) -> Tuple domain -> range
  :=
  match domain with
  | nil => fun f x => f
  | d :: ds =>
    let Apprec := App' ds range in
    match ds as ds0 return
      ((Arrow ds0 range) -> Tuple ds0 -> range) ->
      (Arrow (d :: ds0) range) -> Tuple (d :: ds0) -> range
    with
    | [] => fun _ f x => f x
    | _ =>
      fun Apprec f t =>
      Apprec (f (fst t)) (snd t)
    end Apprec
  end.
*)

Fixpoint App'' (domain : list Type) (range : Type) {struct domain}
         : (Rarrow domain range) -> Rtuple domain -> range
  :=
  match domain with
  | nil => fun f x => f
  | d :: ds =>
    let Apprec := App'' ds (d -> range) in
    match ds return
      ((Rarrow ds (d -> range)) -> Rtuple ds -> d -> range) ->
      (Rarrow (d :: ds) range) -> Rtuple (d :: ds) -> range
    with
    | [] => fun _ f x => f x
    | _ => fun Apprec f t => Apprec f (fst t) (snd t)
    end Apprec
  end.

Eval compute in
  let domain := [D;C;B;A] in
  (fun f : Rarrow domain nat =>
  (fun p : Rtuple domain =>
    Let _ _ p (fun p' => App'' _ _ f p'))).