Parameter P : forall A, (A -> nat) -> Prop.

Hypothesis L1 : forall A, P A (fun x => 0).
Hypothesis L2 : forall A f g, P A f -> P A g -> P A (fun x => f x + g x).

Goal P (nat * nat) (fun '(n,m) => 0).
  Fail apply L1.
Abort.

Goal P (nat * nat) (fun '(n,m) => n + m).
  Fail apply L2.
Abort.