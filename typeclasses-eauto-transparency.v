Record type : Type := Pack { sort : Type }.
Coercion sort : type >-> Sortclass.

Definition nat_packed := Pack nat.

Parameter R : forall A, (A -> nat) -> Prop.
Hypothesis L : forall A, R A (fun _ : A => O).

Hint Resolve L : myhints.
Hint Transparent nat_packed sort : myhints.

Hint Resolve L : lemma.
Hint Transparent nat_packed sort : transp.

Goal R nat_packed (fun _ : nat => O).
  solve [ typeclasses eauto with myhints ]. Restart.
  Fail typeclasses eauto with transp lemma.
  Fail typeclasses eauto with lemma transp.
Abort.