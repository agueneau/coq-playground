Parameter P : forall A, (A -> nat) -> Prop.
Hypothesis P_add : forall A f g,
  P A f -> P A g -> P A (fun x => f x + g x).

Goal P (nat * nat) (fun '(x,y) => x + y).
  Fail apply P_add.
Abort.

Goal P (nat * nat) (fun p => (fst p) + (snd p)).
  apply P_add.
Abort.

Set Primitive Projections.
Unset Printing Primitive Projection Compatibility.
(* required for projections to actually be displayed *)

Record myprod A B  := mypair { myfst : A; mysnd : B }.

Arguments mypair {A B} _ _.
Arguments myfst {A B} _.
Arguments mysnd {A B} _.

Notation "x ** y" := (myprod x y) (at level 80): type_scope.
Notation "( x , y , .. , z )" := (mypair .. (mypair x y) .. z).

Goal P (nat ** nat) (fun '(x,y) => x + y).
  apply P_add.