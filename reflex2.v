Require Import ZArith.
Open Scope Z_scope.
Require Import List.
Require Import Template.All.
Import ListNotations MonadNotation.
Require String.

Notation "s1 =s s2" := (String.string_dec s1 s2) (at level 100).

(*
Inductive expr :=
| E0 : expr
| EAdd : expr -> expr -> expr
| EMax : expr -> expr -> expr
| ETerm : term -> expr.
*)

(*
Fixpoint reflect (t: term): option expr :=
  match t with
  | tApp (tConst "Coq.ZArith.BinIntDef.Z.add" []) [x; y] =>
    ex <- reflect x ;;
    ey <- reflect y ;;
    ret (EAdd ex ey)
  | tApp (tConst "Coq.ZArith.BinIntDef.Z.max" []) [x; y] =>
    ex <- reflect x ;;
    ey <- reflect y ;;
    ret (EMax ex ey)
  | tInd {| inductive_mind := "Coq.Numbers.BinNums.Z"; inductive_ind := 0 |} [] =>
    ret E0
  | _ =>
    ret (ETerm t)
  end.
*)

(* Fixpoint reflect (t: term): option expr := *)
(*   match t with *)
(*   | tApp (tConst fname []) [x; y] => *)
(*     if fname =s "Coq.ZArith.BinIntDef.Z.add" then ( *)
(*       ex <- reflect x ;; *)
(*       ey <- reflect y ;; *)
(*       ret (EAdd ex ey) *)
(*     ) else if fname =s "Coq.ZArith.BinIntDef.Z.max" then ( *)
(*       ex <- reflect x ;; *)
(*       ey <- reflect y ;; *)
(*       ret (EMax ex ey) *)
(*     ) else ( *)
(*       ret (ETerm t) *)
(*     ) *)
(*   | tInd {| inductive_mind := iname; inductive_ind := 0 |} [] => *)
(*     if iname =s "Coq.Numbers.BinNums.Z" then ret E0 *)
(*     else ret (ETerm t) *)
(*   | _ => *)
(*     ret (ETerm t) *)
(*   end. *)

Inductive expr :=
| EVar : expr
| E0 : expr
| EAdd : expr -> expr -> expr
| EMul : expr -> expr -> expr
| EMax : expr -> expr -> expr
| EAbstr : Z -> expr.

Ltac reflect_term var term cont :=
  lazymatch term with
  | tApp (tConst "Coq.ZArith.BinIntDef.Z.add" []) [?x; ?y] =>
    reflect_term var x ltac:(fun e1 =>
    reflect_term var y ltac:(fun e2 =>
    cont constr:(EAdd e1 e2)))
  | tApp (tConst "Coq.ZArith.BinIntDef.Z.mul" []) [?x; ?y] =>
    reflect_term var x ltac:(fun e1 =>
    reflect_term var y ltac:(fun e2 =>
    cont constr:(EMul e1 e2)))
  | tApp (tConst "Coq.ZArith.BinIntDef.Z.max" []) [?x; ?y] =>
    reflect_term var x ltac:(fun e1 =>
    reflect_term var y ltac:(fun e2 =>
    cont constr:(EMax e1 e2)))
  | tInd {| inductive_mind := "Coq.Numbers.BinNums.Z"; inductive_ind := 0 |} [] =>
    cont constr:(E0)
  | var =>
    cont constr:(EVar)
  | ?x =>
    denote_term x ltac:(fun dx =>
    cont constr:(EAbstr dx))
  end.

Ltac reflect var term cont :=
  quote_term term ltac:(fun t => reflect_term var t cont).

Definition combine {A B C}
  (f: A -> B -> C)
  (l1: list A) (l2: list B): list C
:=
  fold_left (fun acc x =>
    fold_left (fun acc y =>
      (f x y) :: acc
    ) l2 acc
  ) l1 [].

Fixpoint explode_max (e: expr): list expr :=
  match e with
  | EAdd e1 e2 =>
    combine EAdd (explode_max e1) (explode_max e2)
  | EMul e1 e2 =>
    combine EMul (explode_max e1) (explode_max e2)
  | EMax e1 e2 =>
    explode_max e1 ++ explode_max e2
  | _ => [e]
  end.

Fixpoint denote (expr: expr) var : Z :=
  match expr with
  | EVar => var
  | E0 => 0
  | EAdd e1 e2 => (denote e1 var) + (denote e2 var)
  | EMul e1 e2 => (denote e1 var) * (denote e2 var)
  | EMax e1 e2 => Z.max (denote e1 var) (denote e2 var)
  | EAbstr z => z
  end.

Fixpoint lforall {A} (P: A -> Prop) (l: list A) :=
  match l with
  | [] => True
  | [x] => P x
  | x :: xs => P x /\ lforall P xs
  end.

Ltac case_max :=
  match goal with |- context [Z.max ?n ?m] =>
    destruct (Z.max_spec_le n m) as [(_ & ->) | (_ & ->)]
  end.

Ltac unpack H :=
  lazymatch type of H with
  | _ /\ _ =>
    let H1 := fresh in
    let H2 := fresh in
    destruct H as [H1 H2];
    unpack H1; unpack H2
  | _ => idtac
  end.

Ltac cut_returning P Q := refine ((_ : P -> Q) _).

Ltac explode_max_tac :=
  match goal with |- ?X = ?rhs =>
    reflect (tRel 0) X ltac:(fun x =>
      cut_returning
        (lforall (fun e' => denote e' 0 = rhs) (explode_max x))
        (denote x 0 = rhs);
      simpl;
      [ try (let H := fresh in intro H; unpack H; repeat case_max; assumption)
      | ]
    )
  end.

Goal forall x y z, x + z = 0 -> x + y * z = 0 -> x + Z.max (Z.max y y * z) z = 0.
intros.
explode_max_tac.
auto.
Qed.

Inductive is_monomial_ret :=
  | WithCoeff : option expr -> is_monomial_ret
  | Const : is_monomial_ret.

Definition emul (e1: option expr) (e2: expr): expr :=
  match e1 with
  | None => e2
  | Some e => EMul e e2
  end.

Definition eadd (e1: option expr) (e2: option expr): expr :=
  let x1 := option_get (EAbstr 1) e1 in
  let x2 := option_get (EAbstr 1) e2 in
  EAdd x1 x2.

Fixpoint is_monomial (e: expr): option is_monomial_ret :=
  match e with
  | EAdd e1 e2 =>
    r1 <- is_monomial e1 ;;
    r2 <- is_monomial e2 ;;
    match (r1, r2) with
    | (Const, Const) => ret Const
    | (WithCoeff c1, WithCoeff c2) => ret (WithCoeff (Some (eadd c1 c2)))
    | _ => None
    end
  | EMul e1 e2 =>
    r1 <- is_monomial e1 ;;
    r2 <- is_monomial e2 ;;
    match (r1, r2) with
    | (WithCoeff c1, WithCoeff c2) => None
    | (WithCoeff c1, Const) => ret (WithCoeff (Some (emul c1 e2)))
    | (Const, WithCoeff c2) => ret (WithCoeff (Some (emul c2 e1)))
    | (Const, Const) => ret Const
    end
  | EMax _ _  => None
  | EVar => ret (WithCoeff None)
  | E0 => ret Const
  | EAbstr _ => ret Const
  end.

Fixpoint explode_add_toplevel (e: expr): list expr :=
  match e with
  | EAdd e1 e2 => explode_add_toplevel e1 ++ explode_add_toplevel e2
  | _ => [e]
  end.

Fixpoint list_split_map {A B C}
  (f: A -> B + C)
  (l: list A)
: list B * list C
:=
  match l with
  | [] => ([], [])
  | x :: xs =>
    let '(l1, l2) := list_split_map f xs in
    match f x with
    | inl a => (a :: l1, l2)
    | inr b => (l1, b :: l2)
    end
  end.

Definition poly1 (e: expr): option (list expr * list expr) :=
  let es := explode_add_toplevel e in
  rs <- monad_map (fun e => r <- is_monomial e ;; ret (e, r)) es ;;
  let '(coeffs, const) := list_split_map (fun '(e, r) =>
    match r with
    | WithCoeff None => inl (EAbstr 1)
    | WithCoeff (Some c) => inl c
    | Const => inr e
    end
  ) rs in
  (* let coeffs := fold_left EAdd coeffs E0 in *)
  (* let const := fold_left EAdd coeffs E0 in *)
  ret (coeffs, const).

Goal forall fuel x y z u,
  1 + (x + (((fuel * y) + z) + u) + ((x * ((y + u * u) * fuel) + fuel + fuel))) = 0.
Proof.
  intros.
  match goal with |- ?X = _ =>
    reflect (tVar "fuel") X ltac:(fun XE =>
      let t := eval cbv in (poly1 XE) in
      pose t
    )
  end.
Abort.

Inductive eqn_op := OpEq | OpLeq.
Inductive eqn := Eqn : eqn_op -> expr -> expr -> eqn.
