Set Typeclasses Filtered Unification.

Parameter Head : Type -> Type.
Parameter P : Type.
Parameter R : Type.
Definition R' := R.
Hypothesis L : Head R -> Head P.

Create HintDb myhints.
Hint Transparent R' : myhints.
Hint Resolve L : myhints.

Goal Head R' -> Head P.
  intro. solve [ eauto with myhints ]. Restart.
  intro. typeclasses eauto with myhints.


  Fail typeclasses eauto with lemma. (* ok *)


  typeclasses eauto with myhints.
  typeclasses eauto with trans lemma.

Abort.
