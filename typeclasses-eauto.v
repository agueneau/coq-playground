Record type : Type := Pack { sort : Type }.
Coercion sort : type >-> Sortclass.

Definition nat_packed := Pack nat.

Parameter R : forall (A : Type), (A -> nat) -> (A -> nat) -> Prop.
Hypothesis R_refl : forall (A : Type) f, R A f f.
Hint Resolve R_refl : myhints.

Goal forall f, R nat_packed f f.
  intro. simple apply R_refl.
Qed. (* ok *)

Goal forall f, R nat_packed f f.
  intro. typeclasses eauto with myhints.
Qed. (* ok *)

Goal R nat_packed (fun x => x) (fun x => x).
  simple apply R_refl.
Qed. (* ok *)

Goal R nat_packed (fun x => x) (fun x => x).
  Fail typeclasses eauto with myhints.
Abort. (* ?? *)

Print HintDb myhints.
(* This seems to replicate the exact pattern that can be found for [R_refl] in
   [myhints]... *)
Hint Extern 0 (R _ ?f ?f) => simple apply R_refl : myhints2.

Goal R nat_packed (fun x => x) (fun x => x).
  typeclasses eauto with myhints2.
Qed. (* ??? *)


Hint Extern 0 (R _ ?f ?f) => simple apply R_refl : hints.
Hint Resolve R_refl : hints'.

Print HintDb hints.
Print HintDb hints'.

Goal R nat (fun (x:nat_packed) => x) (fun (x:nat) => x).
  Fail typeclasses eauto with hints.
  Fail typeclasses eauto with hints'.
  Fail progress (eauto with hints).
  solve [eauto with hints'].
Qed.
