(* Motivation: we are elaborating a term while stepping through a proof. Going
   forward in the proof gives us more information to define the term we are
   elaborating.

   For this example, assume that the term we want to elaborate step-by-step is
   an integer in nat. Initially, we do not know anything about it, so we define
   it as an "evar" (a Coq metavariable). Our proof will start with a:

     evar (x : nat).


   Assume that after stepping a bit through the proof, we got some more
   information about [x], and know that it is of the form [1 + _].

   The goal is to refine the definition of [x], so that it becomes of the form
   "x := 1 + ?x'".
*)


(* We start by defining some Ltac helpers. *)

(* First, given a definition [x := ... ?foo ...], we need to be able to fetch
   the evar ?foo. We can assume the name x to be known, but not the name of the
   evar itself.
*)

(* For a definition of the form [id := id_def] in the context, [get_body id]
   returns the body of the definition, that is [id_def].
*)
Ltac get_body id :=
  let id' := constr:(id) in
  let body := (eval unfold id in id') in
  body.

(* For a definition of the form [id := id_def] in the context, where [id_def]
   contains one evar, [get_evar_in id] returns the term corresponding to that
   evar.

   If [id_def] contains multiple evars, the first one is returned, in the order
   followed by [match .. with context [..] end].
*)
Ltac get_evar_in id :=
  constr:(ltac:(
    let body := get_body id in
    match body with
      context [?E] => is_evar E; exact E
    end
  )).

(* Let's test that it works as intended. *)
Goal True.
  evar (x : nat). pose (test := 1 + x). unfold x in test.
  let X := get_evar_in test in pose (z := X).  (* z := ?x *)
  trivial. Unshelve. exact 0.
Qed.


(* Recall that our goal is to turn a definition [x := ?x] into [x := 1 +
   ?x']. This means instantiating the evar [?x] by [1 + ?x'], where [?x'] is a
   newly created evar.

   Let's try creating [?x'], then using the [instantiate] tactic.

   One issue we could foresee is that, because [?x'] will be created after [?x],
   it may not be allowed in an instantiation for [?x].
*)

Set Printing Existential Instances.

Goal True.
  evar (x : nat).
  evar (x' : nat).
  let X' := get_body x' in instantiate (x := 1 + X').

  (* This seems to be working. Note however that the evar [?x'] used in the
     definition of [x] has [x] itself in its context, which is ill-scoped. Using
     [x] in an "invalid" way will not fail immediately, but will result in an
     ill-scoped proof-term, and Qed will fail.
  *)

  (* Additionnally, it makes some tactics fail, e.g. [unfold x in *] results in
     a Stack Overflow. *)
  unfold x in *.

  trivial. Unshelve. exact 0.
Qed.

(* There is an alternative way to make this work using [instantiate], that does
   not introduces an ill-scoped definition en the context. *)

Goal True.
  evar (x : nat).

  revert x.
  evar (x' : nat).
  intro x.
  let X' := get_body x' in instantiate (x := 1 + X').
  (* Here, the context of the evar in [x]'s definition doesn't mention [x]. *)

  clear x'.
  (* Do some cleanup. *)

  trivial. Unshelve. exact 0.
Qed.

(* This method doesn't introduce ill-scoped terms in the context that confuse
   tactics. However, it is still possible to get ill-scoped proof-terms: *)

Goal True.
  evar (x : nat).
  assert (y : nat). exact x.

  revert x.
  evar (x' : nat).
  intro x.
  let X' := get_body x' in instantiate (x := 1 + X').

  clear x'.

  trivial. Unshelve. exact y.
  (* "No more subgoals." *)

(* Qed fails: "No such section variable or assumption: y. *)
Abort.


(* Finally, a third method is possible, which uses [unify] instead of
   [instantiate]. This seems to work as intended, without direct drawbacks. *)

Goal True.
  evar (x : nat).
  evar (x' : nat).
  let X' := get_body x' in unify x (1 + X').

  (* What seems to be happening here is that, because unification works in both
     directions, both evars in [x] and [x'] get modified to have matching
     contexts. The context of [x'] doesn't mention [x] anymore, and therefore
     can appear in the definition of [x]. *)

  clear x'.
  (* Do some cleanup. *)

  trivial. Unshelve. exact 0.
Qed.
