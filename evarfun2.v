Set Printing Existential Instances.

Goal forall u : unit, exists e, e = fun 'tt => tt.
  intro. eexists.
  destruct u.
  Fail reflexivity.
Abort.

Goal forall (u : unit) (n : nat), exists e, forall (x : unit), e x = n.

  eexists. intros.
  Fail destruct x; reflexivity.
  Fail destruct u; reflexivity.
  reflexivity.

Qed.
