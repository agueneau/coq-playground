Require Import Omega.

Definition foo := 3.

Lemma foo_nonneg : 0 < foo.
Proof. unfold foo. omega. Qed.

Hint Extern 0 (0 < foo) => apply foo_nonneg : foo_base.

Goal 0 < foo.
  auto with foo_base.