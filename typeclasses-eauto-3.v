Record type : Type := Pack { sort : Type }.
Coercion sort : type >-> Sortclass.

Definition nat_packed := Pack nat.

Parameter R : forall (A : Type), (A -> nat) -> (A -> nat) -> Prop.
Hypothesis R_refl : forall (A : Type) f, R A f f.

Hint Resolve R_refl : myhints.

Hint Resolve R_refl | (R _ _ _) : myhintspat.

Hint Extern 0 (R _ ?f ?f) => simple apply R_refl : hints.
Hint Resolve R_refl : hints'.

Hint Transparent sort nat_packed : myhints.
Hint Transparent sort nat_packed : myhintspat.
Print HintDb hints.
Print HintDb hints'.

Goal R nat (fun (x:nat_packed) => x) (fun (x:nat) => x).
  Set Typeclasses Debug Verbosity 2.
  Fail typeclasses eauto with hints.
  Fail typeclasses eauto with hints'.
  Fail progress (eauto with hints).
  typeclasses eauto with myhints.
  Undo.
  Set Typeclasses Filtered Unification.
  Fail typeclasses eauto with myhints.
  typeclasses eauto with myhintspat.
Qed.
