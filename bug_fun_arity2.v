Hypothesis L: forall (x : nat * nat) (f g : nat -> nat -> nat),
    f (fst x) (snd x) = g (fst x) (snd x) ->
    (let (y,z) := x in f y z) = (let (y,z) := x in g y z).

Hypothesis LL: forall A B (f g : A -> B),
    (forall x, f x = g x) ->
    True.

Goal True.
  Set Printing Existential Instances.
  refine (let f1 := (fun '(x, y) => _) : (nat * nat) -> nat in _).
  refine (let f2 := (fun '(x, y) => _) : (nat * nat) -> nat in _).

  apply (@LL _ _ f1 f2).
  intro x. eapply L. simpl. (* simpl. *)
Abort.
