Require Import ZArith.
Require Import Psatz.
Local Open Scope Z_scope.
Require Import Coq.Setoids.Setoid.
Require Import Coq.Classes.Morphisms.

Program Instance proper_Zplus: Proper (Z.le ++> Z.le ++> Z.le) Z.add.
Next Obligation. do 6 intro. omega. Qed.

Goal (forall a b c d, a <= b -> c + b <= c + d -> c + a <= c + d).
  intros ? ? ? ? a_le_b cb_le_cd.
  rewrite a_le_b.
  assumption.
Qed.

Program Instance proper_Zmult_left:
  forall x, 0 <= x ->
  Proper (Z.le ++> Z.le) (Z.mul x).
Next Obligation. do 3 intro. nia. Qed.

Hint Extern 100 (0 <= _) => assumption : typeclass_instances.

Goal forall a b c d, a <= b -> 0 <= c -> c * b <= c * d -> c * a <= c * d.
  intros ? ? ? ? a_le_b O_le_c cb_le_cd.
  rewrite a_le_b.
  assumption.
Qed.

Program Instance proper_Zmult_right:
  forall y, 0 <= y ->
  Proper (Z.le ++> Z.le) (fun x => Z.mul x y).
Next Obligation. do 3 intro. nia. Qed.

Goal forall a b c d, a <= b -> 0 <= c -> b * c <= d * c -> a * c <= d * c.
  intros ? ? ? ? a_le_b O_le_c bc_le_dc.
  (* rewrite a_le_b. *)
Abort.
