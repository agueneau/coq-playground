Require Export Coq.Setoids.Setoid.
Require Export Coq.Classes.Morphisms.
Require Import ZArith.

(* multiplication on Z requires a positivity condition.
   For rewriting on the right, the following work-around works... *)
Definition ShowLater (A : Type) := A.

Hint Extern 100 (ShowLater _) =>
  (unfold ShowLater; shelve) : typeclass_instances.

Program Instance proper_Zmult_left :
  forall x, ShowLater (Z.le 0 x) ->
  Proper (Z.le ++> Z.le) (Z.mul x).
Next Obligation. Admitted.

Open Scope Z_scope.

Goal forall a n n',
  0 <= a ->
  n <= n' ->
  a * n <= a * n'.
Proof.
  intros ? ? ? H1 H2.
  rewrite H2. Focus 2. (* hummmmm *) destruct do_subrelation. assumption.
  apply Z.le_refl.
Fail Qed.
(* Error: No such section variable or assumption: do_subrelation. *)