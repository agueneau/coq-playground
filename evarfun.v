Set Printing Existential Instances.

Parameter P : nat -> Prop.
Parameter E : nat -> nat -> Prop.
Hypothesis Erefl : forall x, E x x.
Hypothesis P0 : forall x, x = 0 -> P x.
Hypothesis PS : forall x y, E y (1+x) -> P x -> P y.
Hypothesis PS' : forall x, P x -> P (1+x).

Hypothesis L : forall (foo : nat -> nat),
  (forall i, P (foo i)) ->
  P (foo 3).

Hypothesis L' : forall (foo : nat -> nat),
  P (foo 42) ->
  P (foo 3).

Goal exists (f: nat * nat -> nat), forall i j, P (f (i, j)).
  match goal with |- ?G =>
    simple refine (let f := (fun pat => let '(x,y) := pat in _) in _);
    try clear pat;
    match goal with |- G => idtac | _ => shelve end
  end.

  exists f. subst f. simpl.
  intros.

  eapply L. intro.
  eapply PS'. (* ok *)
Abort.

Goal exists (f: nat * nat -> nat), forall i j, P (f (i, j)).
    refine (let f := (fun pat => let '(x,y) := pat in _) in _).

  exists f. subst f. simpl.
  intros.

  eapply L. intro.
  eapply PS'. (* wtf *)
Abort.

Goal exists (f: nat * nat -> nat), forall i j, P (f (i, j)).
  match goal with |- ?G =>
    simple refine (let f := (fun pat => let '(x,y) := pat in _) in _);
    try clear pat;
    match goal with |- G => idtac | _ => shelve end
  end.

  exists f. subst f. simpl.
  intros.

  eapply L'.
  eapply PS'. (* wtf *)
Abort.




Lemma I : forall A B (f : A -> B) x y, f = (fun _ => y) -> f x = y.
Proof. intros. subst. reflexivity. Qed.


Goal exists (f: nat * nat -> nat), forall i j, P (f (i, j)).
(* match goal with |- ?G => *)
(*   unshelve (refine (let f := (fun pat => let '(x,y) := pat in _) in _)); *)
(*   try clear pat; *)
(*   match goal with |- G => idtac | _ => shelve end *)
(* end. *)

  refine (let f := (fun '(x,y) => _) in _).

  exists f. subst f. simpl.
  intros.

  eapply L.
  intro.
  (* eapply PS'. *)
  eapply PS. (* reflexivity. *)

  match goal with |- E (?c _) _ =>
    pose (x := c);
    unshelve instantiate (1 := _) in (Value of x)
  end.
  refine (fun _ => _). shelve. hnf. apply Erefl.

  apply P0.
  eapply I. Fail reflexivity.

  Unshelve. 2: clear pat. reflexivity.
Qed.
